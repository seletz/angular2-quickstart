/**
 * Created by seletz on 03.05.16.
 */
import {bootstrap}    from '@angular/platform-browser-dynamic';
import {AppComponent} from './app.component';

bootstrap(AppComponent);
