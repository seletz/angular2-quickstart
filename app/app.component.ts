/**
 * Created by seletz on 03.05.16.
 */
import {Component} from '@angular/core';

@Component({
    selector: 'my-app',
    template: '<h1>First Angular 2 App</h1>'
})
export class AppComponent { }
